import React from 'react'

function Table({
    data
}) {
    return (
        <table>
            <tbody>
                {data.map((value) => <tr key={`tr_${value.title}`}>
                    <td>{value.title}</td>
                    <td>{value.description}</td>
                </tr>)}
            </tbody>
        </table>

    )
}

export default Table;