import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'
import HomePage from '../Home'
import AboutPage from '../About'

function App() {
//   const history = createBrowserHistory()
  return (
    <Router>
        <div>
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home Page</Link>
                    </li>
                    <li>
                        <Link to="/about">about</Link>
                    </li>
                </ul>
            </nav>

            <Switch>
                <Route path="/" exact component={HomePage} />
                <Route path="/about" exact component={AboutPage} />
            </Switch>
        </div>
    </Router>
  );
}

export default App;
