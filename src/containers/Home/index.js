import React, { useState, useEffect } from 'react';
import logo from '../../logo.svg';
import './Home.css';

import Table from '../../components/Table'

function Home() {
  const [data, setData] = useState([])

  const getStatus = async () => {
    const res = await fetch(`http://localhost:3001/v1/books`, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
    })
    const response = await res.json()
    setData(response.data)
  }

  useEffect(() => {
    getStatus()
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload this now.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <Table data={data} />
      </header>
    </div>
  );
}

export default Home;
